

function addListener(object,targetObj,curentList){

object.addEventListener('dblclick',function(event){
                const current = event.currentTarget.objRef;
                const clone = current.cloneNode(true);
                const destList = event.currentTarget.otherSelectRef;
                
                current.remove();
                
                addListener(clone,curentList,targetObj);
                destList.appendChild(clone)
                
                
                //document.location.reload();
            },false)
            object.objRef = object;
            object.otherSelectRef = targetObj; 

}

function initOptionClickEvent(id,otherid){
    var node = document.getElementById(id);
    var otherNode = document.getElementById(otherid);
    var opts = node.childNodes;
 
    for (var i = 0, opt; opt = opts[i]; i++) {
        if (opt.tagName == 'OPTION') {
            
            addListener(opt,otherNode,node);
        }
    }


}

window.addEventListener('load',function(){
initOptionClickEvent("s1","s2");
initOptionClickEvent("s2","s1");
})

