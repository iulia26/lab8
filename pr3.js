const TAB_TEMPLATE_ID = "tabRow";
const TAB_MATRIX_ID = "myMatrix";
function createTable(value) {
  myMatrixValues = [];
  const myTable = document.getElementById(TAB_MATRIX_ID);
  var cellValue = 0;
  var stack = [];
  for (let i = 0; i < value; i++) {
    const template = document.getElementById(TAB_TEMPLATE_ID);
    const templateClone = template.content.cloneNode(true);
    const templateContainer = templateClone.querySelector("tr");
    let innerValue = cellValue;
    let line = [];
    
    for (let j = 0; j < value; j++) {
      const cellData = document.createElement("td");
      cellData.innerHTML = "X";
      cellData.realValue = innerValue;
      cellData.ref = cellData;

      cellData.addEventListener("click", function (event) {
        const hiddenValue = event.currentTarget.realValue;
        const cellRef = event.currentTarget.ref;
        cellRef.innerHTML = hiddenValue.toString();
        stack.push(hiddenValue);

        setTimeout(() => {
          lastOne = stack[stack.length - 1];
          previousLast = stack[stack.length - 2];
      
          console.log(lastOne); 
          console.log(previousLast);
          console.log(stack);
          console.log("=-----------=");
          // console.log(stack);
          if(lastOne != undefined && previousLast != undefined){
                if(lastOne === previousLast){
                    cellRef.innerHTML = hiddenValue.toString();
                    
                }
                else{
                    cellRef.innerHTML = "X";
                }
          }else{
            cellRef.innerHTML = "X";
        }
        }, 2000);
      });

      line.push(innerValue);

      templateContainer.appendChild(cellData);
      if (j % 2 != 0) innerValue = innerValue + 1;
    }
    myMatrixValues.push(line);
    myTable.appendChild(templateContainer);
    cellValue = cellValue + value;
  }
}

window.addEventListener("load", function () {
  createTable(6);
});
