
function sortAfterPosition(position,list){
    for(let i = 0;i<list.length - 1;i++){
        for (let j=i+1;j<list.length;j++){
            let val1  = list[i][position];
            let val2 = list[j][position];
            if(!isNaN(val1)){
                val1 = parseInt(val1);
                val2 = parseInt(val2);
            }
            if(val1 > val2){
                let aux = list[i];
                list[i] = list[j];
                list[j] = aux;
            }
        }
    }
   
    return list;


}

function createRealTab(tabid){
    let table = document.getElementById(tabid);
    let oldtab = [];
    for (const row of table.rows) {  
        let curentRow = [];
        let idx = 0;
        for (const cell of row.cells) {
           
            if(idx != 0){
                curentRow.push(cell.innerHTML);
            }


            idx = idx + 1;
        }
        oldtab.push(curentRow);

    }
    
    let copy = [];
    for(let i = 0;i<oldtab[0].length;i++){
        let row = []
        for(let j = 0;j<oldtab.length;j++){
            row.push('0');
        }
        copy.push(row);
    }
   
        
        for(let i = 0;i<oldtab.length;i++){
            for(let j = 0;j<oldtab[0].length;j++){
               
                copy[j][i] = oldtab[i][j];
            }
        }
      return copy;
    
    


}
function reloadTab(columns,tabid){
    let i = 0;
   console.log(columns);
    let table = document.getElementById(tabid);
    
    for (const row of table.rows) {
             let j = 0;
             indx = 0;
            
            for(const cell of row.cells){
               
                    if(indx != 0){
                        
                        cell.innerHTML = columns[j][i];

                    j  = j + 1;
                    }
                    indx = indx + 1;
            }
            i = i + 1;

    }
  


}
function prepareHandlers(tabid){
    
    let rowList = createRealTab(tabid);
    
    let table = document.getElementById(tabid);
    let cnt = 0;
    for (const row of table.rows) {  
        let idx = 0;
 
        for (const cell of row.cells) { 
             if(idx == 0){
                 
                cell.index = cnt;
                cell.tabid = tabid;
                
                
                cell.addEventListener('click',function(event){
                    const position = event.currentTarget.index;
                    const myTabId = event.currentTarget.tabid;
                    let res = sortAfterPosition(position,rowList);
                    reloadTab(res,myTabId);
                    

                });
               
             }
            idx = idx + 1;
        }
        cnt = cnt + 1;  
        
    }
   
}


window.addEventListener('load', function () {
    prepareHandlers("tab1");
    prepareHandlers("tab2");
  });