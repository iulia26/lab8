
function validateEmail(email){
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
      return true;
    }
      
      return false;
}

function validateForm() {   
    let isValid = true;
  let errorString = "";
  var nume = document.getElementById("nume").value;
  var userdob = document.getElementById("dob").value;
  var age = document.getElementById("age").value;
  var email = document.getElementById("email").value;
  if (nume === "") {
    errorString = errorString.concat("Invalid name\n");
    document.getElementById("nume").className = "error";
    isValid = false;
}
  else{
      document.getElementById("nume").className = "";
        
    }
    if(userdob === ""){
        errorString =  errorString.concat("Invalid date of birth\n");
        isValid = false;
        document.getElementById("dob").className = "error";
    }
    else{

        document.getElementById("dob").className = "";
    }
    if(email === "" || !validateEmail(email)){
        errorString = errorString.concat("Invalid email\n");
        document.getElementById("email").className = "error";
        isValid = false;
    }
    else{
        document.getElementById("email").className = "";
    }
    if(age == ""){
        errorString = errorString.concat("Invalid age\n");
        document.getElementById("age").className = "error";
        isValid = false;
    }
    else{
        document.getElementById("age").className = "";
    }
    if(isValid){
        alert("Date introduse corect");
    }
    else{
        alert(errorString);
    }
    return isValid;


}
window.addEventListener('load',function(){
   
})